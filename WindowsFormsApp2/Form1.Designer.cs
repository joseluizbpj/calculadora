﻿namespace WindowsFormsApp2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Quatro = new System.Windows.Forms.Button();
            this.Cinco = new System.Windows.Forms.Button();
            this.Seis = new System.Windows.Forms.Button();
            this.Sete = new System.Windows.Forms.Button();
            this.Oito = new System.Windows.Forms.Button();
            this.Nove = new System.Windows.Forms.Button();
            this.Um = new System.Windows.Forms.Button();
            this.Dois = new System.Windows.Forms.Button();
            this.Tres = new System.Windows.Forms.Button();
            this.Zero = new System.Windows.Forms.Button();
            this.Igual = new System.Windows.Forms.Button();
            this.Soma = new System.Windows.Forms.Button();
            this.Sub = new System.Windows.Forms.Button();
            this.mult = new System.Windows.Forms.Button();
            this.divi = new System.Windows.Forms.Button();
            this.Ponto = new System.Windows.Forms.Button();
            this.CE = new System.Windows.Forms.Button();
            this.C = new System.Windows.Forms.Button();
            this.Resu = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // Quatro
            // 
            this.Quatro.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Quatro.Location = new System.Drawing.Point(12, 200);
            this.Quatro.Name = "Quatro";
            this.Quatro.Size = new System.Drawing.Size(75, 72);
            this.Quatro.TabIndex = 0;
            this.Quatro.Text = "4";
            this.Quatro.UseVisualStyleBackColor = true;
            this.Quatro.Click += new System.EventHandler(this.b_click);
            // 
            // Cinco
            // 
            this.Cinco.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cinco.Location = new System.Drawing.Point(103, 200);
            this.Cinco.Name = "Cinco";
            this.Cinco.Size = new System.Drawing.Size(75, 72);
            this.Cinco.TabIndex = 1;
            this.Cinco.Text = "5";
            this.Cinco.UseVisualStyleBackColor = true;
            this.Cinco.Click += new System.EventHandler(this.b_click);
            // 
            // Seis
            // 
            this.Seis.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Seis.Location = new System.Drawing.Point(199, 200);
            this.Seis.Name = "Seis";
            this.Seis.Size = new System.Drawing.Size(75, 72);
            this.Seis.TabIndex = 2;
            this.Seis.Text = "6";
            this.Seis.UseVisualStyleBackColor = true;
            this.Seis.Click += new System.EventHandler(this.b_click);
            // 
            // Sete
            // 
            this.Sete.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Sete.Location = new System.Drawing.Point(12, 320);
            this.Sete.Name = "Sete";
            this.Sete.Size = new System.Drawing.Size(75, 72);
            this.Sete.TabIndex = 3;
            this.Sete.Text = "7";
            this.Sete.UseVisualStyleBackColor = true;
            this.Sete.Click += new System.EventHandler(this.b_click);
            // 
            // Oito
            // 
            this.Oito.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Oito.Location = new System.Drawing.Point(103, 320);
            this.Oito.Name = "Oito";
            this.Oito.Size = new System.Drawing.Size(75, 72);
            this.Oito.TabIndex = 4;
            this.Oito.Text = "8";
            this.Oito.UseVisualStyleBackColor = true;
            this.Oito.Click += new System.EventHandler(this.b_click);
            // 
            // Nove
            // 
            this.Nove.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Nove.Location = new System.Drawing.Point(199, 320);
            this.Nove.Name = "Nove";
            this.Nove.Size = new System.Drawing.Size(75, 72);
            this.Nove.TabIndex = 5;
            this.Nove.Text = "9";
            this.Nove.UseVisualStyleBackColor = true;
            this.Nove.Click += new System.EventHandler(this.b_click);
            // 
            // Um
            // 
            this.Um.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Um.Location = new System.Drawing.Point(12, 86);
            this.Um.Name = "Um";
            this.Um.Size = new System.Drawing.Size(75, 72);
            this.Um.TabIndex = 6;
            this.Um.Text = "1";
            this.Um.UseVisualStyleBackColor = true;
            this.Um.Click += new System.EventHandler(this.b_click);
            // 
            // Dois
            // 
            this.Dois.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dois.Location = new System.Drawing.Point(103, 86);
            this.Dois.Name = "Dois";
            this.Dois.Size = new System.Drawing.Size(75, 72);
            this.Dois.TabIndex = 7;
            this.Dois.Text = "2";
            this.Dois.UseVisualStyleBackColor = true;
            this.Dois.Click += new System.EventHandler(this.b_click);
            // 
            // Tres
            // 
            this.Tres.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Tres.Location = new System.Drawing.Point(199, 86);
            this.Tres.Name = "Tres";
            this.Tres.Size = new System.Drawing.Size(75, 72);
            this.Tres.TabIndex = 8;
            this.Tres.Text = "3";
            this.Tres.UseVisualStyleBackColor = true;
            this.Tres.Click += new System.EventHandler(this.b_click);
            // 
            // Zero
            // 
            this.Zero.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Zero.Location = new System.Drawing.Point(310, 271);
            this.Zero.Name = "Zero";
            this.Zero.Size = new System.Drawing.Size(84, 53);
            this.Zero.TabIndex = 9;
            this.Zero.Text = "0";
            this.Zero.UseVisualStyleBackColor = true;
            this.Zero.Click += new System.EventHandler(this.b_click);
            // 
            // Igual
            // 
            this.Igual.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Igual.Location = new System.Drawing.Point(310, 339);
            this.Igual.Name = "Igual";
            this.Igual.Size = new System.Drawing.Size(177, 53);
            this.Igual.TabIndex = 10;
            this.Igual.Text = "=";
            this.Igual.UseVisualStyleBackColor = true;
            this.Igual.Click += new System.EventHandler(this.Igual_Click);
            // 
            // Soma
            // 
            this.Soma.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Soma.Location = new System.Drawing.Point(310, 129);
            this.Soma.Name = "Soma";
            this.Soma.Size = new System.Drawing.Size(87, 56);
            this.Soma.TabIndex = 11;
            this.Soma.Text = "+";
            this.Soma.UseVisualStyleBackColor = true;
            this.Soma.Click += new System.EventHandler(this.op_click);
            // 
            // Sub
            // 
            this.Sub.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Sub.Location = new System.Drawing.Point(400, 129);
            this.Sub.Name = "Sub";
            this.Sub.Size = new System.Drawing.Size(87, 56);
            this.Sub.TabIndex = 12;
            this.Sub.Text = "-";
            this.Sub.UseVisualStyleBackColor = true;
            this.Sub.Click += new System.EventHandler(this.op_click);
            // 
            // mult
            // 
            this.mult.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mult.Location = new System.Drawing.Point(310, 200);
            this.mult.Name = "mult";
            this.mult.Size = new System.Drawing.Size(87, 56);
            this.mult.TabIndex = 13;
            this.mult.Text = "*";
            this.mult.UseVisualStyleBackColor = true;
            this.mult.Click += new System.EventHandler(this.op_click);
            // 
            // divi
            // 
            this.divi.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.divi.Location = new System.Drawing.Point(403, 200);
            this.divi.Name = "divi";
            this.divi.Size = new System.Drawing.Size(87, 56);
            this.divi.TabIndex = 14;
            this.divi.Text = "/";
            this.divi.UseVisualStyleBackColor = true;
            this.divi.Click += new System.EventHandler(this.op_click);
            // 
            // Ponto
            // 
            this.Ponto.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Ponto.Location = new System.Drawing.Point(400, 271);
            this.Ponto.Name = "Ponto";
            this.Ponto.Size = new System.Drawing.Size(87, 53);
            this.Ponto.TabIndex = 15;
            this.Ponto.Text = ",";
            this.Ponto.UseVisualStyleBackColor = true;
            this.Ponto.Click += new System.EventHandler(this.b_click);
            // 
            // CE
            // 
            this.CE.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CE.Location = new System.Drawing.Point(310, 86);
            this.CE.Name = "CE";
            this.CE.Size = new System.Drawing.Size(87, 27);
            this.CE.TabIndex = 16;
            this.CE.Text = "CE";
            this.CE.UseVisualStyleBackColor = true;
            this.CE.Click += new System.EventHandler(this.CE_Click);
            // 
            // C
            // 
            this.C.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.C.Location = new System.Drawing.Point(400, 86);
            this.C.Name = "C";
            this.C.Size = new System.Drawing.Size(87, 27);
            this.C.TabIndex = 17;
            this.C.Text = "C";
            this.C.UseVisualStyleBackColor = true;
            this.C.Click += new System.EventHandler(this.C_Click);
            // 
            // Resu
            // 
            this.Resu.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Resu.Location = new System.Drawing.Point(38, 12);
            this.Resu.Multiline = true;
            this.Resu.Name = "Resu";
            this.Resu.Size = new System.Drawing.Size(452, 68);
            this.Resu.TabIndex = 18;
            this.Resu.Text = "0";
            this.Resu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(499, 427);
            this.Controls.Add(this.Resu);
            this.Controls.Add(this.C);
            this.Controls.Add(this.CE);
            this.Controls.Add(this.Ponto);
            this.Controls.Add(this.divi);
            this.Controls.Add(this.mult);
            this.Controls.Add(this.Sub);
            this.Controls.Add(this.Soma);
            this.Controls.Add(this.Igual);
            this.Controls.Add(this.Zero);
            this.Controls.Add(this.Tres);
            this.Controls.Add(this.Dois);
            this.Controls.Add(this.Um);
            this.Controls.Add(this.Nove);
            this.Controls.Add(this.Oito);
            this.Controls.Add(this.Sete);
            this.Controls.Add(this.Seis);
            this.Controls.Add(this.Cinco);
            this.Controls.Add(this.Quatro);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Calculadora";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Quatro;
        private System.Windows.Forms.Button Cinco;
        private System.Windows.Forms.Button Seis;
        private System.Windows.Forms.Button Sete;
        private System.Windows.Forms.Button Oito;
        private System.Windows.Forms.Button Nove;
        private System.Windows.Forms.Button Um;
        private System.Windows.Forms.Button Dois;
        private System.Windows.Forms.Button Tres;
        private System.Windows.Forms.Button Zero;
        private System.Windows.Forms.Button Igual;
        private System.Windows.Forms.Button Soma;
        private System.Windows.Forms.Button Sub;
        private System.Windows.Forms.Button mult;
        private System.Windows.Forms.Button divi;
        private System.Windows.Forms.Button Ponto;
        private System.Windows.Forms.Button CE;
        private System.Windows.Forms.Button C;
        private System.Windows.Forms.TextBox Resu;
    }
}

