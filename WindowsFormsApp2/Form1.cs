﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        double resultado = 0;
        String op = "";
        public Form1()
        {
            InitializeComponent();
        }

        private void b_click(object sender, EventArgs e)

        {
            if(Resu.Text == "0")
            {
                Resu.Clear();//elimina o Zero inicial
            }
            Button bt = (Button)sender; //casta o sender para um botão
            Resu.Text = Resu.Text + bt.Text;//concatena o texto do text box com o texto do botão
        }

        private void op_click(object sender, EventArgs e)
        {
            Button bt = (Button)sender;
            op = bt.Text;//salva o operador em op
            resultado = Double.Parse(Resu.Text);//passa o resultado para um double
            Resu.Clear();
        }

        private void CE_Click(object sender, EventArgs e)
        {
            Resu.Text = "0"; //zera o texto na text box
            //teste do repos
        }

        private void C_Click(object sender, EventArgs e)
        {
            Resu.Text = "0";
            resultado = 0; //zera a "memoria"
        }

        private void Igual_Click(object sender, EventArgs e)
        {
            switch(op)
            {
                case "+":
                    Resu.Text = (resultado + Double.Parse(Resu.Text)).ToString();
                    break;
                case "-":
                    Resu.Text = (resultado - Double.Parse(Resu.Text)).ToString();
                    break;
                case "*":
                    Resu.Text = (resultado * Double.Parse(Resu.Text)).ToString();
                    break;
                case "/":
                    Resu.Text = (resultado / Double.Parse(Resu.Text)).ToString();
                    break;
            }
        }
    }
}
